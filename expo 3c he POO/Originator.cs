﻿using System;

namespace expo_3c_he_POO
{
    [Serializable]
    class Originator
    {
        private string nombredelpropietario;
        private string modelodelcarro;
        private int añodelcarro;
        private double costo;

        public string Nombredelpropietario { get => nombredelpropietario; set => nombredelpropietario = value; }
        public string Modelodelcarro { get => modelodelcarro; set => modelodelcarro  = value; }
        public int Añodelcarro { get => añodelcarro; set => añodelcarro = value; }
        public double Costo { get => costo; set => costo = value; }

        // en el contructor pasamos lo que es el nombre, modelo,costo 
        // y colocamos en las variables internas 
        public Originator(string pNombredelpropietario, string pModelodelcarro,  int pAñodelcarro, double pCosto)
        {
            nombredelpropietario = pNombredelpropietario;
            modelodelcarro = pModelodelcarro;
            añodelcarro= pAñodelcarro;
            costo = pCosto;
        }

        // procedemos a crear un metodo mostrar 
        public void Mostrar()
        {
            Console.WriteLine(" {0} {1} {2}, con un costo de {3}", nombredelpropietario, modelodelcarro, añodelcarro, costo);

        }
        public Memento CreaMemento()
        {
            Memento miMemento = new Memento();
            miMemento.Salvar(this);
            return miMemento;
        }

        public void Restaurar(Memento pMemento)
        {
            Originator temp = pMemento.Restaurar();
            nombredelpropietario = temp.Nombredelpropietario;
            modelodelcarro = temp.Modelodelcarro;
            añodelcarro = temp.Añodelcarro;
            costo = temp.Costo;

        }


    }
}

