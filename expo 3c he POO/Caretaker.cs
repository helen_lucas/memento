﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace expo_3c_he_POO
{
    // creamos la clase careteaker
    class Caretaker
    {
        // pocedemos a colocar una variable la cual se llamara memento 
        private Memento memento;
        // posteriormente hemos creado una propiedad 
        public Memento Memento { get => memento; set => memento = value; }
    }
}

