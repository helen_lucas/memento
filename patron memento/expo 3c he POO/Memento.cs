﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace expo_3c_he_POO
{ 
    public class Memento
    {
        internal void Salvar(Originator objeto)
        {
            BinaryFormatter Formateador = new BinaryFormatter();
            Stream miStream = new FileStream("Autos.pro", FileMode.Create, FileAccess.Write, FileShare.None);
            Formateador.Serialize(miStream, objeto);
            miStream.Close();
            // acontinuacion mandamos un mensaje 
            Console.WriteLine("Nos hemos salvado");
        }
        internal Originator Restaurar()
        {
            BinaryFormatter formateador = new BinaryFormatter();
            Stream miStream = new FileStream("Autos.pro", FileMode.Open, FileAccess.Read, FileShare.None);
            Originator temp = (Originator)formateador.Deserialize(miStream);
            miStream.Close();

            // mandamos un mensaje diciendo que se ha restaurado 
            Console.WriteLine("Se ha restaurado");

            // procedemos a regresar ese objeto que tiene el estado 
            return temp;

        }
    }

}
