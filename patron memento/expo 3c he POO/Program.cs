﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace expo_3c_he_POO
{
     public class Program
     {
        static void Main(string[] args)
        {
            //a continuacion creamos el Originator
            Originator auto = new Originator(" jose Lucas", "Nissan 4x4", 2019, 35000);
            auto.Mostrar();
             //
            //luego procedemos a crear el Caretaker
            Caretaker TI = new Caretaker();
            
            //procedemos a guardar el estado en este caso sera Memento
            TI.Memento = auto.CreaMemento();

            //modificamos nuestro objeto 
            auto.Nombredelpropietario = "Hector Lucas ";
            auto.Modelodelcarro = "Nissan 4x4 ";
            auto.Añodelcarro = 1980;
            auto.Costo = 25000;
            auto.Mostrar();

            //procedemos a restaurar el estado anterior 
            auto.Restaurar(TI.Memento);
            auto.Mostrar();

        }
    }
}

